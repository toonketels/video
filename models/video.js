/**
 * Mongodb video model.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
 
var videoSchema = new Schema({
    title:  {type: String, required: true},
    description: String,
    url: {type: String, required: true, match: /^http:\/\/.*\.[a-z]{2,3}\/.*$/i},
    type: {type: String, required: true, match: /^(youtube||vimeo)$/},
    postdate: {type: Date, default: Date.now},
    data: [{footnote: {text: String, start: Number, end: Number}}]
});
 
module.exports = mongoose.model('Video', videoSchema);