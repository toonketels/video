/**
 * GET users listing.
 */
var Video = require('../models/video')
    errors = require('../utils/errors');


/**
 * Return all videos.
 */
var list = function(req, res, next){

  Video.find(function(err, data){
    if (err) {
      var error = new errors.LocateResourceError('There was an error getting the video collection from mongo');
      error.prev = err;
      return next(error);
    }

    res.send(data);
  });
};


/**
 * Return a single video by id.
 */
var show = function(req, res, next) {
  // req.params.id;
  // req.query
  // req.body

  var id = req.params.id;

  Video.findById(id, function(err, data) {
    if (err) {
      var error = new errors.LocateResourceError('There was an error getting the video with id '+id+ ' from mongo');
      error.prev = err;
      return next(error);
    }

    res.send(data);
  });
};


/**
 * Store a video in the tb.
 */
var create = function(req, res, next) {
  var model = req.body,
      video = new Video(model);

  console.log(video);

  video.save(function(err, data) {
    if (err) {
      var error = new errors.CreateResourceError('There was an error creating a video ' + JSON.stringify(video) );
      error.prev = err;
      return next(error);
    }

    // Only send responds when video is really saved...
    // We need to send back the model with the id
    res.send(data);
  });  
};


/**
 * Update a video.
 */ 
var update = function(req, res, next) {
  var id = req.params.id,
      attributes = req.body;

  // Mongo does not like it when we set the id again,
  // event with the same value
  delete attributes._id;

  Video.findByIdAndUpdate(id, {$set: attributes}, function(err, data) {
    if (err) {
      var error = new errors.UpdateResourceError('Error updating model with id '+id+' to mongo');
      error.prev = err;
      return next(error);
    }

    res.send(data);
  });
};


/**
 * Delete a video.
 */
var destroy = function(req, res, next) {
  var id = req.params.id;

  Video.findOneAndRemove(id, function(err, data) {
    if (err) {
      var error = new errors.DestroyResourceError('Error destroy model with id '+id+' in mongo');
      error.prev = err;
      return next(error);
    }

    res.send(204);
  })
};

exports.list = list;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;