/**
 * App error handling middleware.
 */

var errorHandler = function(err, req, res, next){
  
  console.log(err);

  if (err.code) {
    switch (err.code) {
      case 50001:
        res.send(404, 'Not found.');
        break;
      default:
        res.send(500, 'Something broke!');
        break;
    }
  }

  // For errors without code...
  res.send(500, 'Something broke!');
};



exports.errorHandler = errorHandler;