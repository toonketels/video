/**
 * Custom errors for our app.
 */


 /**
  * Whenever we have an issue updating a resource.
  */
function UpdateResourceError(message) {
  this.name = "UpdateResourceError";
  this.message = message || "Error updating a resource";
  this.code =  50000;
}
UpdateResourceError.prototype = new Error();
UpdateResourceError.prototype.constructor = UpdateResourceError;



/**
 * Unable to locate a resource.
 */
function LocateResourceError(message) {
  this.name = "LocateResourceError";
  this.message = message || "Error locating a resource";
  this.code =  50001;
}
LocateResourceError.prototype = new Error();
LocateResourceError.prototype.constructor = LocateResourceError;



/**
 * Unable to Create a resource.
 */
function CreateResourceError(message) {
  this.name = "CreateResourceError";
  this.message = message || "Error creating a resource";
  this.code =  50003;
}
CreateResourceError.prototype = new Error();
CreateResourceError.prototype.constructor = CreateResourceError;


/**
 * Unable to Destroy a resource.
 */
function DestroyResourceError(message) {
  this.name = "DestroyResourceError";
  this.message = message || "Error destroying a resource";
  this.code =  50003;
}
DestroyResourceError.prototype = new Error();
DestroyResourceError.prototype.constructor = DestroyResourceError;

exports.UpdateResourceError = UpdateResourceError;
exports.LocateResourceError = LocateResourceError;
exports.CreateResourceError = CreateResourceError;
exports.DestroyResourceError = DestroyResourceError;