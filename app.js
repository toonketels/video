
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    video = require('./routes/video'),
    http = require('http'),
    path = require('path'),
    mongoose = require('mongoose');

var app = express();

// Connect with mongo

mongoose.connect('mongodb://localhost/video');

// Check on mongo error

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Error handling middleware to send correct http response codes
app.use(require('./services/error-handler').errorHandler);


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/**
 * app.param("id")nto automatically load video
 * http://expressjs.com/api.html#app.param
 */

app.get('/video/:id', video.show);
app.get('/video', video.list);
app.post('/video', video.create);
app.put('/video/:id', video.update);
app.delete('/video/:id', video.destroy);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
